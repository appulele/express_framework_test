var express = require('express');
var mysql = require('mysql');
var router = express.Router(); 
var isDesktop = true;
/* GET home page. */


//mobile Check
function is_mobile(req) {
    var ua = req.header('user-agent');
    if (/mobile/i.test(ua)) return true;
    else return false;
};
 

 

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' ,index:1 ,isDesktop: isDesktop});
});

router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Home' ,index:1,isDesktop: !is_mobile(req)});
});

router.get('/diary', function(req, res, next) {
  res.render('diary', { title: 'Diary' ,index:2,isDesktop: isDesktop});
});

router.get('/dream', function(req, res, next) {
  res.render('index', { title: 'Dream' ,index:3,isDesktop: isDesktop});
});

router.get('/portfolio', function(req, res, next) {
    /*  mysql
        host, posrt, user, password, database, debug
  */
  var client = mysql.createConnection({
    user:'root',
    password:'green5569',
    database:'Portfolios'
  });

  var portfolios;
  var portfolio_images;
  client.query('USE Portfolios');
  
  

  client.query('SELECT * FROM portfolios', function (error, result, fields) {
      if (error) {
        console.log("쿼리문 오류");
      }else{
        console.log("포트폴리오=====================");
        console.log(result);
        portfolios = result;
        console.log("query result=====================");

        client.query('SELECT * FROM portfolio_images', function (error, resultP, fields) {
        if (error) {
          console.log("쿼리문 오류");
        }else{
          console.log("포트폴리오 이미지=====================");
          console.log(resultP);
          portfolio_images = resultP;
          console.log("query result=====================");
          res.render('portfolios', { title: 'Portfolio' ,index:4,isDesktop: isDesktop, portfolios: portfolios,portfolio_images:portfolio_images});
        };
      });
        // res.render('portfolios', { title: 'Portfolio' ,index:4,isDesktop: isDesktop, portfolios: portfolios,portfolio_images:portfolio_images});
      };
    });
});

router.get('/ability', function(req, res, next) {
  res.render('index', { title: 'Ability' ,index:5,isDesktop: isDesktop});
});

router.get('/guest', function(req, res, next) {
  res.render('index', { title: 'Guest' ,index:6,isDesktop: isDesktop});
});

router.get('*', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

module.exports = router;
